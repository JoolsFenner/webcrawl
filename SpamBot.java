import java.net.*;
import java.io.*;
import java.util.Set;
import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.*;


public class SpamBot implements Runnable {
	private static int webPageCrawlCount;
	private static int threadCount;
	private static Set<String> masterLinkSet  = new HashSet<String>();
	private static BlockingQueue<String> urlQueue = new LinkedBlockingQueue<String>();
	private static Set<String> masterEmailSet = new HashSet<String>();
	private static boolean finished = false;
	
	/*public SpamBot(){
		this.masterLinkSet = new HashSet<String>();
		this.masterEmailSet = new HashSet<String>();
		this.urlQueue = new LinkedBlockingQueue<String>();
	}*/
	
	
	//void setSeed(String seedUrl) throws MalformedURLException;
	
	
	public void run(){
		while(urlQueue.size()==0)
			try{
				Thread.sleep(1000);
				//wait();
				
			} catch (InterruptedException ex){
				System.out.println("Interrupted Exception");
			}
		
		crawlPage();
		
	}
	
	
	private void crawlPage(){
		threadCount++;
		do{
			WebPageImpl webPageCrawl = new WebPageImpl(urlQueue.poll());
			webPageCrawlCount++;
			
			System.out.println();
			System.out.println(webPageCrawlCount);
			System.out.println("Next Link In Queue = " + urlQueue.peek());
			System.out.println("Size of queue = " + urlQueue.size());
			System.out.println("Size of email list = " + masterEmailSet.size());
				
			webPageCrawl.parseWebPage();
			
			masterEmailSet.addAll(webPageCrawl.getEmailSet());
			for (String url : webPageCrawl.getLinkSet()) {
				if (!masterLinkSet.contains(url)) {
					masterLinkSet.add(url);
						try{
							urlQueue.put(url);
						}catch(InterruptedException ex){
							System.out.println(ex);
						}
				}
			}	
			//notifyAll();
		} while(masterEmailSet.size() <= 20);
		if (!finished) {
			finished = true;
			writeToFile();
		}
		
	}
	
	public synchronized void writeToFile(){

		File file = new File("emails.txt");
		PrintWriter out = null;
		try {
			out = new PrintWriter(file);
			Iterator it = masterEmailSet.iterator();
			
			for(String next : masterEmailSet){
				out.write(next);
				out.write(System.getProperty("line.separator"));
			}
			
		} catch (FileNotFoundException ex) {
		System.out.println("Cannot write to file " + file + ".");
		} finally {
			out.close();
		}	
	}	

	
	private void launch(String seedUrl){
		SpamBot sb = new SpamBot();
		try{ 
			urlQueue.put(seedUrl);
		} catch(InterruptedException ex){
			System.out.println(ex);
		}
		for (int i = 0; i < 4; i++) {
			SpamBot spamBotTask = new SpamBot();
			Thread t = new Thread(spamBotTask);
			t.start();
		}
		
	}
	
	public static void main(String[] args){	
		new SpamBot().launch(args[0]);
			}
	
}