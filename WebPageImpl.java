import java.net.*;
import java.io.*;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;

public class WebPageImpl {
	private String url;
	private Set<String> linkSet = new HashSet<String>();
	private Set<String> emailSet = new HashSet<String>();
	
	
	public WebPageImpl(String url){
		this.url = url;
	}
	
	public void parseWebPage(){
	
		BufferedReader in = null;
		try {
			URL newurl = new URL(url);
			in = new BufferedReader(new InputStreamReader(newurl.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if(inputLine.contains("<a href=\"http")) {
					int startIndex = inputLine.indexOf("<a href=\"http")+9;
					linkSet.add(inputLine.substring(startIndex,inputLine.indexOf("\"", startIndex)));
				} else if (inputLine.contains("<a href=\"mailto")){
					int startIndex = inputLine.indexOf("<a href=\"mailto")+16;
					emailSet.add(inputLine.substring(startIndex,inputLine.indexOf("\"", startIndex)));
				}
			}
        } catch(IOException ex){
			System.out.println(ex);
		} finally{
			closeReader(in);
		}
	}
	private void closeReader(Reader reader){
		try{
		if(reader != null){
			reader.close();
		}
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}
	
	public Set<String> getLinkSet(){
		return linkSet;
	}
	public Set<String> getEmailSet(){
		return emailSet;
	}
	
	
}